function getSearchParameters() {
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

var params = getSearchParameters();


function renderUsers(users) {
    let usersList = document.getElementById('usersList');
    usersList.innerHTML = '';
    
    for(let user of users) {
        let div = document.createElement('div');
        div.innerHTML = user;
        usersList.append(div);
    }
}

let socket = new WebSocket("ws://localhost:3000");

socket.onopen = () => {
    console.log("Соединение установлено");
    socket.send(
        JSON.stringify({
            userName: params.name
        })
    );
};

socket.onmessage = function(event) {
    try {
        let dataParsed = JSON.parse(event.data);
        renderUsers(dataParsed);
    } catch(e) {
        console.log(event.data);
    }
    
};

socket.onclose = function(event) {
    if (event.wasClean) {
        console.log(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
    } else {
        console.log('[close] Соединение прервано');
    }
};

socket.onerror = function(error) {
    console.log(`[error] ${error.message}`);
};