const WebSocket = new require('ws');
const port = 3000;

const webSocketServer = new WebSocket.Server({ 
    port: port
 });

let clients = [];

webSocketServer.on('connection', (ws) => {
    let id = getUserId(clients);
    clients[id] = {
        info: {},
        connection: ws
    };

    ws.on('message', message => {
        try {
            let dataParsed = JSON.parse(message);
            clients[id].info = Object.assign( clients[id].info, dataParsed );
            sendMessage( getUsers(clients), clients );
        } catch(e) {
            sendMessage(message, clients);
        }
    });
 
    ws.on("error", e => ws.send(e));

    ws.on('close', function() {
        console.log('соединение закрыто ' + id);
        delete clients[id];
    });
});

function getUserId(clients) {
    let id = Math.random();
    if(clients[id]) {
        return getUserId(clients);
    }

    return id;
}

function getUsers(clients) {
    let result = [];
    for (let key in clients) {
        result.push( clients[key].info.userName );
    }

    return JSON.stringify(result);
}

function sendMessage(message, clients) {
    for (let key in clients) {
        clients[key].connection.send(message);
    }
}